# CICD Architecture

## Overview

This project is an enterprise production ready cd\cd platform. This doc describes its architecture.

Live CD\CD system access is possble upon request.

Source code, AWS Console and Terraform modules demos are available with screencast.

## Requirements

Requirements as follows:

* Build enterprise CI\CD platform.
* High scaleability.
* High availability.
* Fault tolerance.
* Distributed.
* Based on OpenSource Jenkins.
* AWS infrastructure.
* Should include pipeline templates for Java, Node.js, .Net.
* Should implenent pipeline as a code principle.
* Notification capabilities.
* Platform health visualisation with a dashboard.
* Pipelines should be able to deploy and publish applications.
* Secure, no secrets in code and configurations.

## HA and Resilience

HA and resilience options as follows:

* Two AZs (data centers) are used to deploy resources.
* ASG is used for ECS node recovery. Node provision is automated with bootstrap scripts.
* ECS service is used to maintain Jenkins instance online.
* EFS is used to store Jenkins data.
* ALB HealChecks are used to determine service status.
* Route53 is used to publish external endpoints.
* AWS CloudMap is used for internal service discovery.
* Automatic provisioning and configuration implemented for ECS nodes and Jenkins slaves.

## Security

Security options as follows:

* Security groups with restricted ingress are used to restrict network access.
* IAM roles and policies are used to restrict access to AWS resources.
* Networks isolation with private and public subnets. Private resources never exposed.
* ALB protected with HTTPS and a valid certificate.

## Elasticity and Scalability

Elasticity and scalability:

* ECS nodes can be scaled up large sizes to accomodate a bigger Jenkins master.
* FARGATE task nodes are automatically scaled out to infinity to handle multiple parallel tasks.
* EFS provides plently of storage space for the master.

## Operations and Maintenance:

* All the infrastructure is IaC (Terraform).
* Docker is used to build Jenkins Slave images.
* Automatic provisioning script implemented for ECS nodes.
* AWS SSM SessionManager is used to get ECS shell access.
* GIT is used so store IaC sources.

## Architecture

### Network

Compute resources are isolated in VPC. EFS mount points, ECS nodes, FARGATE tasks are executed in private subnets. ALB endpoints are deployed to public subnets.

![Network Layout](images/cicd-arch-net.png)

### Jenkins

Jenkins uses ECS EC2 to handle the master and ECS FARGATE to handle slaves.

![Network Layout](images/cicd-arch-jenkins.png)

### IaC and Configuration management

**Terraform layers as follows:**

| Layer | Repo | Notes |
| --- | --- | --- |
| network | tf-btlabz-cicd-jenkins-layer | VPC, gateways, routes, subnets etc |
| security | tf-btlabz-cicd-jenkins-layer | Security groups, ACLs |
| jenkins | tf-btlabz-cicd-jenkins-layer | ECS, ALB, Master and Slaves |

**Terraform modules as follows:**

| Module | Notes |
| --- | --- |
| terraform-aws-btlabz-vpc-pub3x-pri3x-ha-nat-module | CICD VPC buildup with service discovery |
| terraform-aws-btlabz-cicd-master-module | Jenkins Master Service and resources |
| terraform-aws-btlabz-cicd-alb-module | Public ALB with listeners and certificates |
| terraform-aws-btlabz-cicd-artifactory-module | Artifactory service |
| terraform-aws-btlabz-cicd-ecr-module | ECR repo with policies |
| terraform-aws-btlabz-cicd-ecs-module | ECS cluster |
| terraform-aws-btlabz-cicd-efs-module | Shared EFS module |
| terraform-aws-btlabz-cicd-node-module | ECS node group |
| terraform-aws-btlabz-cicd-slave-module | Jenkins  FarGate Slaves |
| terraform-aws-btlabz-cicd-sonar-module | SonarQUbe Service |
| terraform-aws-btlabz-nat-base-module | NAT gateway |
| terraform-aws-btlabz-pri-sn-module | Private subnet |
| terraform-aws-btlabz-pub-sn-module | Public subnet |
| terraform-aws-btlabz-vpc-base-module | VPC itself and basiv resources |

All the terraform modules are hosted in the private Terraform Cloud and used as such.

**Docker images:**

| Image | Notes |
| --- | --- |
| docker-btlabz-cicd-jnlp-slave | Jenkins JNLP slave for Java\Maven |

### Services

Master, Dashboard, SonarQube, Artifactory and other services are handled by the same ECS EC2.

| Service | Notes |
| --- | --- |
| master | Jenkins Master Service |
| sonar | SonarQube Service |
| artifactory | JFrog Artifactory Service |

## ToDo

Basic requirements are satisfied. Nevertheless, here is a list of action items.

### Pipeline development

Basic templates for Java are provides. Nevertheless ToDo:

* codify configurations and use seed dsl
* Java demo should use shared artifact storage
* add microservice deployment to ECS
* codify configurations and use seed dsl
* refactor Jenkinsfile to use shared pipeline libraries
* add Node.JS samples
* add Windows slaves, .Net samples

### Infrastructure development:

* add SSO (Cognito, CloudDirectory, AD etc) to access CICD services
* add EC2 cloud to build docker images
* add Inspector, WAF, Config, Antivirus, HIDS
* add OS Patching process
* use Packer to produce ECS AMIs to speed up the bootstrap process
* SonarQube data persistance (at the moment ephemeral)
* Predictive ECS slaves scaling (pre-warmed slave pool)

### Maintenance development

* FarGate Stale Slaves cleanup and detection
* EFS Backup to S3
* Metrics and logs publishing

### Security development

* Use SSM to store secrets.
* Add patch management and ecs node recycling.
* Add AWS Inspector to ECS nodes.
* Add AC and HIDS to ecs nodes.
* Add CloudTrail+Athena on CI\CD Logs
